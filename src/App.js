import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './App.css';
import HomePage from "./lib/pages/home/HomePage";
import LoginPage from "./lib/pages/auth/LoginPage";

const App = () => {

  // Do some initialization

  return (
    // Route to different pages
    <Router>
      <Switch>
        <Route exact path={'/'}>
          <HomePage/>
        </Route>
        <Route exact path={'/login'}>
          <LoginPage/>
        </Route>

      </Switch>
    </Router>
  );
};

export default App;

