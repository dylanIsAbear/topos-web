
import React, {useState} from 'react';
import NavMenu from "../../components/nav_menu/NavMenu";
import './home_page.css';
import Modal from 'react-modal';
import PrivacyPolicyModal from "../../components/privacy_policy/PrivacyPolicy";

const HomePage = (props) => {

  const [showPrivacyModal, setShowPrivacyModal] = useState(false);

  const navMenuList = [
    {
      text: 'Privacy Policy',
      onClick: () => setShowPrivacyModal(!showPrivacyModal),
    },
    {
      text: 'Support',
      onClick: () => window.location.href = '/'
    },
    {
      text: 'About us',
      onClick: () => window.location.href = '/'
    },
    {
      text: 'Create a Account',
      onClick: () => window.location.href = '/'
    },
  ];

  const privacyModalStyle = {

  };

  return (
    <div className={'topos-home-page'}>
      <NavMenu imageUrl={'/logo/logo.png'} itemList={navMenuList}/>
      <Modal
        isOpen={showPrivacyModal}
        onRequestClose={()=>setShowPrivacyModal(false)}
        style={{
          overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(255, 255, 255, 0.75)'
          },
          content: {
            position: 'absolute',
            top: '30px',
            left: '150px',
            right: '150px',
            bottom: '40px',
            border: '0px solid #ccc',
            background: '#fff',
            overflow: 'scroll',
            WebkitOverflowScrolling: 'touch',
            borderRadius: '8px',
            outline: 'none',
            padding: '20px'
          }
        }}
      >
        <PrivacyPolicyModal/>
      </Modal>
    </div>)
};

export default HomePage;