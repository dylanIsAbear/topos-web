import React from "react";
import './nav_menu.css'

const NavMenuItem = (props) => {

  const {
    key,
    item,
  } = props;

  if(!item) throw Error('nav menu item cannot be null!');

  return (
     <div className={'nav-menu-item'} onClick={item.onClick}>
        {item.text}
     </div>
  );

};

const NavMenu = (props) => {

  /**
   *
   * @item {
   *   text : string,
   *   onClick : Function(void)
   * }
   *
   */

  const { // Receive the props
    imageUrl,
    itemList, // align from right
  } = props;



  if(!imageUrl){throw Error('image cannot be null');}
  if(!itemList){throw Error('itemList cannot be null!');}

  return(
    <div className={'nav-menu'}>
      <img className={'nav-menu-image'} alt={'logo'} src={imageUrl}/>
      <div className={'nav-menu-items'}>
        {
          itemList.map((item, index) => <NavMenuItem key={index} item={item}/>)
        }
      </div>
    </div>
  );
};

export default NavMenu;